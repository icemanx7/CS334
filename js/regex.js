var ck_name = /^[A-Za-z0-9 \-]{2,20}$/;
var ck_surnname = /^[A-Za-z0-9 ]{2,20}$/;
var ck_email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i 
var ck_phone = /^[0-9_]{10}$/;

function validate(myForm){
  var name = myForm.Name.value;
  var errors = [];

  if ( !ck_name.test(document.myForm.Name.value )) {
    document.myForm.Name.focus() ;
    document.myForm.Name.style.borderColor = "red" ;
    errors[errors.length] = "You valid Name .";
  }
  if (!ck_surnname.test(document.myForm.Surname.value)) {
    document.myForm.Surname.focus() ;
    document.myForm.Surname.style.borderColor = "red" ;
    errors[errors.length] = "You must enter a valid Surname ";
  }
  if (!ck_email.test(document.myForm.Email.value)) {
    document.myForm.Email.focus() ;
    document.myForm.Email.style.borderColor = "red" ;
    errors[errors.length] = "You must enter a valid email address.";
  }
  if (!ck_phone.test(document.myForm.Phone.value)) {
    document.myForm.Phone.style.borderColor = "red" ;
    document.myForm.Phone.focus() ;
    errors[errors.length] = "Please enter a valid phone number";
  }
  if (errors.length > 0) {

    reportErrors(errors);
    return false;

  }
  return true;

}

function changeColor(myForm) {
  myForm.style.borderColor = "green" ;
}

function reportErrors(errors){
  var msg = "Please Enter Valide Data \n";
  var printMes = document.getElementById("errors");

  for (var i = 0; i<errors.length; i++) {
    var numError = i + 1;
    msg += "\n" + " | " + numError +" " + errors[i];

  }
  printMes.style.fontSize = "10px";
  printMes.style.color = "red";
  printMes.innerHTML = msg;
  // alert(msg);

}
