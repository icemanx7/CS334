function validate()
{
  var re = "^[a-zA-Z0-9]*[a-zA-Z]+[a-zA-Z0-9]*$";

  if( document.myForm.Name.value == "" )
  {
    alert( "Please provide your name!" );
    document.myForm.Name.focus() ;
    document.myForm.Name.style.borderColor = "red" ;
    return false;
  }

  if( document.myForm.Surname.value == "" )
  {
    alert( "Please provide your Surname!" );
    document.myForm.Surname.focus() ;
    document.myForm.Surname.style.borderColor = "red" ;
    return false;
  }

  if( document.myForm.Email.value == "" )
  {
    alert( "Please provide your Email!" );
    document.myForm.Email.focus() ;
    document.myForm.Email.style.borderColor = "red" ;
    return false;
  }

  if( document.myForm.Phone.value == "" )
  {
    alert( "Please provide your Phone Number!" );
    document.myForm.Phone.style.borderColor = "red" ;
    document.myForm.Phone.focus() ;
    return false;
  }

  // if( document.myForm.Zip.value == "" ||
  //     isNaN( document.myForm.Zip.value ) ||
  //     document.myForm.Zip.value.length != 5 )
  // {
  //   alert( "Please provide a zip in the format #####." );
  //   document.myForm.Zip.focus() ;
  //   return false;
  // }

  if( document.myForm.Country.value == "-1" )
  {
    alert( "Please provide your country!" );
    return false;
  }
  return( true );
}
