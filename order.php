<!DOCTYPE html>

<?php
require_once('database.php');
if ($_SERVER["REQUEST_METHOD"] == "POST"){
	/* if ($_SERVER["REQUEST_METHOD"] == "POST"){ */
		$databases = new Database();
		$databases->show_all();

		#This is for the CSV Writing
		$name = $_POST["Name"]; 
		$surname = $_POST["Surname"]; 
		$email = $_POST["Email"]; 
		$phone = $_POST["Phone"]; 
		$databases->insertCustomer($name,$surname,$email,$phone,"Rick");
		$databases->show_all();
		$outData = array();
		$outData[] = array('Name' => "$name" ,'Surname' => "$surname" , 'Email' => "$email", 'Phone' => "$phone");
		$json = json_encode($outData);
		echo $json;
		$filepointer = fopen('/tmp/testForm.csv','a');
		fwrite($filepointer,$json);
		fclose($filepointer);
	/* } */
}
?>

<html>

 <!-- Including the style sheet -->
 <head>

  <link rel="stylesheet" type="text/css" href="styles/index.css">
  <link rel="stylesheet" type="text/css" href="styles/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="styles/mobile.css" media="screen and (max-width:480px)">
  <script src="js/regex.js"></script>
  <script src="js/jquery-1.12.2.min.js"></script>
  <script src="js/bootstrap.js"></script>
  <script src="js/bootstrap.min.js"></script>

 </head>

 <div class="row">
  <div	class="logo"	>
   <img	src="images/logo.png"	class="start"	alt="Mountain	View">
  </div>	
 </div>

 <nav class="navbar navbar-default" role="navigation">

  <div class="navbar-header">
   <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
   </button>
  </div>

  <div class="collapse navbar-collapse" id="example-navbar-collapse">

   <ul class="nav navbar-nav">
    <li><a href="home.html">Home</a></li>
    <li><a href="products.html">Products</a></li>
    <li><a href="order.html">Order</a>
     <li><a href="contact.html">Contact</a>
     </li>
   </ul>
  </div>
 </nav>

 <div class="row">
  <div class="col-md-2"></div>
  <div class="col-md-8">
   <h1	class="title">Order</h1>	
   <hr	style="width:	100%;	color:	#729c23;	height:	1px;	background-color:#729c23"	/>

			<div>
				<p id="errors">
				<!-- Richard Thomas -->
				</p>
			</div>
   <div class="col-md-4">

    <form class="form-horizontal" name="myForm" role="form" onsubmit="return validate(this)" method="post" action="order.php">

     <div class="form-group">
      <label class="control-label col-md-3" for="names">Name:</label>
      <div class="col-md-9">
       <input type="name" class="form-control" name="Name" id="name" onchange="changeColor(this)"  placeholder="Enter firstname" >
      </div>
     </div>

     <div class="form-group">
      <label class="control-label col-md-3" for="surnames">Surname:</label>
      <div class="col-md-9">
       <input type="surname" class="form-control" name="Surname" onchange="changeColor(this)" placeholder="Enter lastname">
      </div>
     </div>

     <div class="form-group">
      <label class="control-label col-md-3" for="email">Email:</label>
      <div class="col-md-9">
       <input type="email" class="form-control" name="Email" id="email" onchange="changeColor(this)" placeholder="Enter email">
      </div>
     </div>

     <div class="form-group">
      <label class="control-label col-md-3" for="phone">Phone Number:</label>
      <div class="col-md-9">
       <input type="phone" class="form-control" name="Phone" id="phone" onchange="changeColor(this)" placeholder="Enter Phone Number">
      </div>
     </div>

     <input type="submit" value="Place Order">
    </form>					
   </div>

   <div class="col-md-4">
    <p  class="ord">
    Lorem	ipsum	dolor	sit	amet,
    consectetur	adipiscing	elit.	Mauris
    egestas	lacinia	arcu	non	tempus.
    Aenean	pellentesque	dolor	id	diam.
    </p>
   </div>
   <div class="col-md-9 table-responsive">

    <table class="table table-bordered vert-align">

     <thead>
      <tr>
       <th>Items</th>
       <th>Quantity</th>
      </tr>
     </thead>

     <tbody>
      <tr>
       <th>Richard</th>
       <td>
        <div class="input-group col-md-3">
         <span class="input-group-btn">
          <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[2]">
           <span class="glyphicon glyphicon-minus"></span>
          </button>
         </span>
         <input type="text" name="quant[2]" class="form-control input-number" value="10" min="1" max="100">
         <span class="input-group-btn">
          <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[2]">
           <span class="glyphicon glyphicon-plus"></span>
          </button>
         </span>
        </div>
       </td>
      </tr>

      <tr>
       <th>Richard</th>
       <td>
        <div class="input-group col-md-3">
         <span class="input-group-btn">
          <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[2]">
           <span class="glyphicon glyphicon-minus"></span>
          </button>
         </span>
         <input type="text" name="quant[2]" class="form-control input-number" value="10" min="1" max="100">
         <span class="input-group-btn">
          <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[2]">
           <span class="glyphicon glyphicon-plus"></span>
          </button>
         </span>
        </div>
       </td>
      </tr>

      <tr>
       <th>Richard</th>
       <td>
        <div class="input-group col-md-3">
         <span class="input-group-btn">
          <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[2]">
           <span class="glyphicon glyphicon-minus"></span>
          </button>
         </span>
         <input type="text" name="quant[2]" class="form-control input-number" value="10" min="1" max="100">
         <span class="input-group-btn">
          <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[2]">
           <span class="glyphicon glyphicon-plus"></span>
          </button>
         </span>
        </div>
       </td>
      </tr>

      <tr>
       <th>Richard</th>
       <td>
        <div class="input-group col-md-3">
         <span class="input-group-btn">
          <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[2]">
           <span class="glyphicon glyphicon-minus"></span>
          </button>
         </span>
         <input type="text" name="quant[2]" class="form-control input-number" value="10" min="1" max="100">
         <span class="input-group-btn">
          <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[2]">
           <span class="glyphicon glyphicon-plus"></span>
          </button>
         </span>
        </div>
       </td>
      </tr>
     </tbody>

    </table>
   </div>
   <hr style="width: 100%; color: #729c23; height: 1px; background-color:#729c23" />
  </div>
 </div>


 <div class="row">
  <div class="col-md-2"></div>
  <div class="col-md-10">
   <h1	class="title">Recommended Products</h1>	

   <div class="row prod rec">
    <div	class="col-md-3">
     <div	class="img">
      <a	target="_blank"	href="images/05.jpg">
       <img	src="images/05.jpg"	alt="Fjords"	width="230"	height="150">
       <figcaption>Lorem Ispum</figcaption>
      </a>
     </div>
    </div>

    <div	class="col-md-3">
     <div	class="img">
      <a	target="_blank"	href="images/06.jpg">
       <img	src="images/06.jpg"	alt="Fjords"	width="230"	height="150">
       <figcaption>Flor sit amet</figcaption>
      </a>
     </div>
    </div>

    <div	class="col-md-3">
     <div	class="img">
      <a	target="_blank"	href="images/07.jpg">
       <img	src="images/07.jpg"	alt="Fjords"	width="230"	height="150">
       <figcaption>Consectetur adipis</figcaption>
      </a>
     </div>
    </div>

    <div	class="col-md-3">
     <div	class="img">
      <a	target="_blank"	href="images/08.jpg">
       <img	src="images/08.jpg"	alt="Fjords"	width="230"	height="150">
       <figcaption>Maurus egestas</figcaption>
      </a>
     </div>
    </div>

   </div>
  </div>
  <div class="col-md-1"></div>
 </div>
 </div>
 <div class="row fot">

  <div class="col-md-12">

   <footer>
    All Rights Reserved
    <img	src="images/logo.png" style="width:8%;"	alt="Mountain	View">
    2016 Trinkets Inc.
   </footer> 

  </div>
 </div>
</html>

